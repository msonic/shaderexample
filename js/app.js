// set the scene size
var WIDTH = 400,
HEIGHT = 300;

// set some camera attributes
var VIEW_ANGLE = 45,
ASPECT = WIDTH / HEIGHT,
NEAR = 0.1,
FAR = 10000;

// get the DOM element to attach to
// - assume we've got jQuery to hand
var $container = $('#container');

// create a WebGL renderer, camera
// and a scene
var renderer = new THREE.WebGLRenderer();
var camera = new THREE.Camera(  VIEW_ANGLE,
	                        ASPECT,
	                        NEAR,
	                        FAR  );
var scene = new THREE.Scene();

// the camera starts at 0,0,0 so pull it back
camera.position.z = 300;

// start the renderer
renderer.setSize(WIDTH, HEIGHT);

// attach the render-supplied DOM element
$container.append(renderer.domElement);


var attributes = {
  displacement: {
    type: 'f', // a float
    value: [] // an empty array
  },
  velocity: { 
    type: 'f',
    value: []
  }
};

var uniforms = {
  amplitude: {
    type: 'f', // a float
    value: 0
  },
  frame: {
    type: 'i',
    value: 0
  }
};

// create the sphere's material
var shaderMaterial = new THREE.MeshShaderMaterial({
  attributes: attributes,
  uniforms: uniforms,
  vertexShader:   $('#vertexshader').text(),
  fragmentShader: $('#fragmentshader').text()
});

// set up the sphere vars
var radius = 50, segments = 16, rings = 16;

// create a new mesh with sphere geometry -
// we will cover the sphereMaterial next!
var sphere = new THREE.Mesh(
  new THREE.Sphere(radius, segments, rings),
  shaderMaterial);

// add the sphere to the scene
scene.addChild(sphere);

// now populate the array of attributes
var verts =
  sphere.geometry.vertices;

var values =
  attributes.displacement.value;

for(var v = 0; v < verts.length; v++) {
  values.push(Math.random() * 30);
}
for (var v = 0; v < sphere.geometry.vertices.length; v++) {
  attributes.velocity.value.push(Math.random());
}

// draw!
renderer.render(scene, camera);

// animation loop
var frame = 0;
function update() {

  // update the amplitude based on
  // the frame value.
  uniforms.amplitude.value =
    Math.sin(frame * 0.1);
  uniforms.frame.value = frame;

  // update the frame counter
  frame += 1;

  renderer.render(scene, camera);

  // set up the next call
  requestAnimationFrame(update);
}

requestAnimationFrame(update);
